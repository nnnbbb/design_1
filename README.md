# Deliverables

## Team name: NB

[Design requirement](./Requirement.md)

## Group members :

Zixiao Wang(001058840), Yuchuan Lin(001302756)

---

### 1. Report outlining your proposed solution.

Through the five years’ data of graduates’ quality of employment, career prospects, subjective evaluation of employers and employers, and objective data of educational impact, to evaluate and   track the growth of all aspects of the graduates within 5 years. The comprehensive feedback learned creates a set of evaluation dimensions for the university education system to measure the performance of university education quality, thus helping to improve the system and serve students.

The final dashboard will rank the colleges’ score from evaluation program.

Following is our settings of evaluation frame:

![table](./table.jpg)

### 2. Sequence diagrams showing how to navigate the university object model to deliver performance metrics needed for performance and feedback.

> Object Model

![o](./WechatIMG2124.png)

> Sequence Model

![s](WechatIMG2122.png)